package com.example.integration.flow.khranovskyi.bean;

public enum OrderState {
    CANCELLED,
    WAITING_FOR_PAYMENT,
    PAYMENT_COMPLETE
}
