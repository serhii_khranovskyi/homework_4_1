package com.example.integration.flow.khranovskyi.integration;

import com.example.integration.flow.khranovskyi.bean.Order;
import com.example.integration.flow.khranovskyi.bean.OrderState;
import org.apache.log4j.Logger;
import org.springframework.integration.annotation.Router;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Splitter;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class Processor {
    private static final Logger LOGGER = Logger.getLogger(Process.class);

    @ServiceActivator(inputChannel = "inputDataChannel", outputChannel = "extractStrings")
    public File getData(File file) {
        LOGGER.trace("inputDataChannel activator is invoked");
        LOGGER.trace(file);
        return file;
    }

    @Splitter(inputChannel = "extractStrings", outputChannel = "parseOrder")
    public List<String> extractStrings(File file) {
        LOGGER.trace("extractStrings activator is invoked");
        List<String> list = new ArrayList<>();
        Scanner scanner;
        try {
            scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                list.add(s);
                LOGGER.trace(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    @ServiceActivator(inputChannel = "parseOrder", outputChannel = "undeliveredOrders")
    public Order parseOrder(String string) {
        LOGGER.trace("parseOrder activator is invoked");
        String[] parameters = string.split(";");
        String name = parameters[0].trim();
        OrderState orderState = OrderState.valueOf(parameters[1].trim());
        return new Order(name, orderState);
    }

    @Router(inputChannel = "sortDeliver", defaultOutputChannel = "orderOutputChannel")
    public String sort(Order order) {
        LOGGER.trace("Order " + order + " is sorted");
        return order.getOrderState() != OrderState.CANCELLED ? "payDeliver" : null;
    }

    @ServiceActivator(inputChannel = "payDeliver", outputChannel = "deliver")
    public Order pay(Order order) {
        if (order.getOrderState() == OrderState.WAITING_FOR_PAYMENT) {
            order.setOrderState(OrderState.PAYMENT_COMPLETE);
            LOGGER.trace("Order " + order + " is payed");
        }
        return order;
    }

    @ServiceActivator(inputChannel = "deliver", outputChannel = "deliveredOrders")
    public Order deliver(Order order) {
        LOGGER.trace("Order " + order + " is delivered");
        return order;
    }
}
