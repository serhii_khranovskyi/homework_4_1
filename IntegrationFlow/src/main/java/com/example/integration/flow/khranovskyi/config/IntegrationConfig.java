package com.example.integration.flow.khranovskyi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.file.FileReadingMessageSource;

import java.io.IOException;

@Configuration
@EnableIntegration
@ComponentScan(basePackages = "com.example.integration.flow.khranovskyi")
@IntegrationComponentScan(basePackages = "com.example.integration.flow.khranovskyi")
public class IntegrationConfig {
    @Bean
    @InboundChannelAdapter(value = "inputDataChannel", poller = @Poller(fixedDelay = "1000"))
    public FileReadingMessageSource fileReadingMessageSource() throws IOException {
        FileReadingMessageSource reader = new FileReadingMessageSource();
        reader.setDirectory(new ClassPathResource("data").getFile());
        return reader;
    }

    @Bean
    public QueueChannel undeliveredOrders() {
        return new QueueChannel();
    }

    @Bean
    public QueueChannel deliveredOrders() {
        return new QueueChannel();
    }

    @Bean
    public QueueChannel orderOutputChannel() {
        return new QueueChannel();
    }
}
