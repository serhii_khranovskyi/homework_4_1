package com.example.integration.flow.khranovskyi;

import com.example.integration.flow.khranovskyi.bean.Order;
import com.example.integration.flow.khranovskyi.config.IntegrationConfig;
import com.example.integration.flow.khranovskyi.integration.ExampleGateway;
import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;

import java.util.ArrayList;
import java.util.List;


public class ApplicationStart {
    private static final Logger LOGGER = Logger.getLogger(ApplicationStart.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(IntegrationConfig.class);
        ExampleGateway gateway = context.getBean("gateway", ExampleGateway.class);
        PollableChannel deliveredOrdersChannel = context.getBean("deliveredOrders", PollableChannel.class);
        PollableChannel undeliveredOrdersChannel = context.getBean("undeliveredOrders", PollableChannel.class);
        List<Order> deliveredOrderList = new ArrayList<>();

        Message<?> reply = undeliveredOrdersChannel.receive();
        while (reply != null) {
            Order order = (Order) reply.getPayload();
            gateway.sortDeliver(order);
            reply = undeliveredOrdersChannel.receive(1000);
        }

        reply = deliveredOrdersChannel.receive();
        while (reply != null) {
            Order order = (Order) reply.getPayload();
            deliveredOrderList.add(order);
            reply = deliveredOrdersChannel.receive(1000);
        }

        context.close();
        LOGGER.trace("Orders " + deliveredOrderList.size() + " was delivered.");
    }

}
