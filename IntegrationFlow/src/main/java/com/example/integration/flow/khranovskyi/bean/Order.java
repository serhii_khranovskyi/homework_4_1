package com.example.integration.flow.khranovskyi.bean;

public class Order {
    //POJO
    private Integer id;
    private String name;
    private OrderState orderState;

    public Order() {
    }

    public Order(String name, OrderState orderState) {
        this.name = name;
        this.orderState = orderState;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }
}
