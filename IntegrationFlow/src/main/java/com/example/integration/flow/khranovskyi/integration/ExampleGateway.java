package com.example.integration.flow.khranovskyi.integration;

import com.example.integration.flow.khranovskyi.bean.Order;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway(name = "gateway")
public interface ExampleGateway {
    @Gateway(requestChannel = "sortDeliver")
    void sortDeliver(Order order);
}


